﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusReservation.Controllers;
using BusReservation.Models;
using System.Web.Mvc;

namespace BusReservation.Test.Controllers
{
    [TestClass]
    public class DestinationsTest
    {
        private DestinationsController obj;
        private BusReservationContext db;
        public ViewModel expected;


        [TestInitialize]
        public void Init() {
            obj = new DestinationsController();
            db = new BusReservationContext();
            expected = new ViewModel();
        }

        [TestMethod]
        public void TestIndex()
        {
            
            var actResult = obj.Index() as ViewResult;

            Assert.IsNotNull(actResult);
        }



        //[TestMethod]
        //public void TestDetails()
        //{
        //    var actResult = obj.Details(1) as ViewResult;
            
        //    expected.Destinations.DestinationId = 1;
        //    expected.Destinations.From = "Skopje";
        //    expected.Destinations.To = "Skopje";
        //    expected.Destinations.PriceOneWay = 300;
        //    expected.Destinations.PriceTwoWay = 400;
        //    expected.Destinations.PriceStudent = 200;
        //    expected.Destinations.BusId = 1;
        //    expected.Destinations.Bus = db.Buses.Find(1);
        //    expected.Tickets = db.Tickets.Where(ticket => ticket.DestinationId == 1).ToList<Ticket>();

        //    Assert.AreEqual(expected, actResult.ViewData.Model);
            
        //}
    }
}