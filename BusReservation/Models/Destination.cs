﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BusReservation.Models
{
    public class Destination
    {
        public int DestinationId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        [DisplayName("Price (One Way)")]
        public int PriceOneWay { get; set; }
        [DisplayName("Price (Two Way)")]
        public int PriceTwoWay { get; set; }
        [DisplayName("Price (Student)")]
        public int PriceStudent { get; set; }
        public int BusId { get; set; }
        
        public Bus Bus { get; set; }
        
        public virtual List<Ticket> Tickets { get; set; }
        

        public Destination()
        {
            Tickets = new List<Ticket>();
        }

        public Destination(int destinationId, string from, string to, int priceOneWay, int priceTwoWay, int priceStudent, Bus bus, List<Ticket> tickets)
        {
            DestinationId = destinationId;
            From = from;
            To = to;
            PriceOneWay = priceOneWay;
            PriceTwoWay = priceTwoWay;
            PriceStudent = priceStudent;
            Tickets = tickets;
            this.Bus = bus;
            string FullNameDestination = From + "-" + To;
            
        }

        
    }
}