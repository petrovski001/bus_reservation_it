﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BusReservation.Models
{
    [JsonObject("Ticket")]
    public class Ticket
    {
        public int TicketId { get; set; }
        public int Type { get; set; }
        public int Price { get; set; }
        public Nullable<DateTime> Date { get; set; }
        [DisplayName("Seat Number")]
        public int SeatNumber { get; set; }
        public int Peron { get; set; }
        public int DestinationId { get; set; }
        
        public virtual Destination Destination { get; set; }
        //[Key]
        public string UserId { get; set; }
        //[ForeignKey("UserId")]
        //public virtual ApplicationUser User { get; set; }

        public Ticket() { }

        public Ticket(int ticketId, int type, int price, int seatNumber, int peron, Destination des)
        {
            TicketId = ticketId;
            Type = type;
            Price = price;
            Date = DateTime.Now;
            SeatNumber = seatNumber;
            Peron = peron;
            Destination = des;
            
        }
    }
}