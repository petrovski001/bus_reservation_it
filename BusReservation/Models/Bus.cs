﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BusReservation.Models
{
    public class Bus
    {
        public int BusId { get; set; }
        public string Name { get; set; }
        [DisplayName("Number Of Seats")]
        public int NumberOfSeats { get; set; }
        
        public virtual ICollection<Ticket> Tickets { get; set; }

        public Bus()
        {
            Tickets = new List<Ticket>();
        }

        public Bus(int busId, string name, int numberOfSeats, ICollection<Ticket> tickets)
        {
            BusId = busId;
            Name = name;
            NumberOfSeats = numberOfSeats;
            Tickets = tickets;
        }
    }
}