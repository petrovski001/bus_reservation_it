﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusReservation.Models;
using Microsoft.AspNet.Identity;

namespace BusReservation.Controllers
{
    public class TicketsController : Controller
    {
        private BusReservationContext db = new BusReservationContext();
        public List<int> list = new List<int>();

        // GET: Tickets
        [Authorize]
        public ActionResult Index()
        {
            var userID = User.Identity.GetUserId();
            var list = db.Tickets.Where(x => x.UserId == userID).ToList();
            return View(list);
        }

        // GET: Tickets/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // GET: Tickets/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Avtobusi = db.Buses.ToList<Bus>();
            ViewBag.Destinacii = db.Destinations.ToList<Destination>();
            
            return View();
        }


        [HttpPost]
        public ActionResult Test(string ReturnedObject)
        {
            Ticket ticket = new Ticket();
            if (ModelState.IsValid)
            {
                if (ReturnedObject != null)
                {
                    var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(ReturnedObject);
                    ticket.Type = int.Parse(JSONObj["Type"]);
                    ticket.Price = int.Parse(JSONObj["Price"]);
                    //ticket.Date = new DateTime(long.Parse(JSONObj["Date"]));
                    ticket.Peron = int.Parse(JSONObj["Peron"]);
                    ticket.DestinationId = int.Parse(JSONObj["DestinationId"]);
                    ticket.UserId = User.Identity.GetUserId();
                    int id = int.Parse(JSONObj["BusId"]);
                    Bus bus = (Bus)db.Buses.Find(id);
                    ticket.Destination = (Destination) db.Destinations.Find(ticket.DestinationId);
                    ticket.Destination.Bus = bus;
                    string[] arr = JSONObj["seatNo"].Split(',');
                    int[] seatsNo = Array.ConvertAll(arr, s => int.Parse(s));
                    if(User.Identity.IsAuthenticated == true)
                    {
                        for (int i = 0; i < seatsNo.Length; i++)
                        {
                            ticket.SeatNumber = seatsNo[i];
                            db.Tickets.Add(ticket);
                            db.SaveChanges();
                        }
                        ticket.Price = ticket.Price * seatsNo.Length;
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }

                    
                    //ticket.bus = JSONObj["BusId"];
                }
                
                
            }

            return View(ticket);
         
        }

        //POST: Tickets/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(string TicketsNo, [Bind(Include = "TicketId,Type,Price,Date,SeatNumber,Peron,DestinationId")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                if (User.Identity.IsAuthenticated)
                {
                    ticket.UserId = User.Identity.GetUserId();
                    string[] arr = TicketsNo.Split(',');
                    db.Tickets.Add(ticket);
                    db.SaveChanges();
                    //Destination destination = db.Destinations.Find(ticket.DestinationId);
                    
                    return RedirectToAction("Index");
                }   
            }

            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "TicketId,Type,Price,Date,SeatNumber,Peron")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
