﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusReservation.Models;

namespace BusReservation.Controllers
{
    public class ViewModel
    {
        public List<Ticket> Tickets { get; set; }
        public Destination Destinations { get; set; }

        public ViewModel()
        {
            Tickets = new List<Ticket>();
            Destinations = new Destination();
        }
    }
}