namespace BusReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DestinationBusForeignKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Destinations", "Bus_BusId", c => c.Int());
            CreateIndex("dbo.Destinations", "Bus_BusId");
            AddForeignKey("dbo.Destinations", "Bus_BusId", "dbo.Buses", "BusId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Destinations", "Bus_BusId", "dbo.Buses");
            DropIndex("dbo.Destinations", new[] { "Bus_BusId" });
            DropColumn("dbo.Destinations", "Bus_BusId");
        }
    }
}
