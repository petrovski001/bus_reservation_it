namespace BusReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Buses",
                c => new
                    {
                        BusId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NumberOfSeats = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BusId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        SeatNumber = c.Int(nullable: false),
                        Peron = c.Int(nullable: false),
                        Destination_DestinationId = c.Int(),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.Destinations", t => t.Destination_DestinationId)
                .Index(t => t.Destination_DestinationId);
            
            CreateTable(
                "dbo.Destinations",
                c => new
                    {
                        DestinationId = c.Int(nullable: false, identity: true),
                        From = c.String(),
                        To = c.String(),
                        PriceOneWay = c.Int(nullable: false),
                        PriceTwoWay = c.Int(nullable: false),
                        PriceStudent = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DestinationId);
            
            CreateTable(
                "dbo.TicketBus",
                c => new
                    {
                        Ticket_TicketId = c.Int(nullable: false),
                        Bus_BusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Ticket_TicketId, t.Bus_BusId })
                .ForeignKey("dbo.Tickets", t => t.Ticket_TicketId, cascadeDelete: true)
                .ForeignKey("dbo.Buses", t => t.Bus_BusId, cascadeDelete: true)
                .Index(t => t.Ticket_TicketId)
                .Index(t => t.Bus_BusId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Destination_DestinationId", "dbo.Destinations");
            DropForeignKey("dbo.TicketBus", "Bus_BusId", "dbo.Buses");
            DropForeignKey("dbo.TicketBus", "Ticket_TicketId", "dbo.Tickets");
            DropIndex("dbo.TicketBus", new[] { "Bus_BusId" });
            DropIndex("dbo.TicketBus", new[] { "Ticket_TicketId" });
            DropIndex("dbo.Tickets", new[] { "Destination_DestinationId" });
            DropTable("dbo.TicketBus");
            DropTable("dbo.Destinations");
            DropTable("dbo.Tickets");
            DropTable("dbo.Buses");
        }
    }
}
